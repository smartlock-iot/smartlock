package hr.ferit.student.smartlock;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;

import static hr.ferit.student.smartlock.App.CHANNEL_ID;

public class NotificationHelper{
    private Context context;

    public NotificationHelper(Context context){
        this.context = context;
    }

    public Notification createNotification(String input){
        //TODO - open tracking end activity
        Intent notificationIntent = new Intent(context, MapActivity.class);
        notificationIntent.putExtra(context.getResources().getString(R.string.nofitication_ssd), true);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        return new NotificationCompat.Builder(context, CHANNEL_ID)
                .setContentTitle("SmartLock service")
                .setContentText(input)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .build();
    }
}
