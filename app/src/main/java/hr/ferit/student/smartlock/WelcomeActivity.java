package hr.ferit.student.smartlock;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;

public class WelcomeActivity extends AppCompatActivity {

    private static final int RC_SIGN_IN = 1;
    public static boolean loggedIn = false;
    Button btnLogin;
    Button btnShowMap;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        if(FirebaseAuth.getInstance().getCurrentUser()!=null) {
            loggedIn = true;
            Intent intent = new Intent(context, MapActivity.class);
            startActivity(intent);
        }
        setup();
    }

    private void setup(){
        btnLogin = findViewById(R.id.btnLogin);
        btnShowMap = findViewById(R.id.btnShowMap);
        btnLogin.setOnClickListener(v -> {
            if(!loggedIn){
                //Login
                startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(Arrays.asList(
                                    new AuthUI.IdpConfig.EmailBuilder().build(),
                                    new AuthUI.IdpConfig.GoogleBuilder().build()))
                            .setTheme(R.style.LoginTheme)
                            .build(),
                    RC_SIGN_IN);
            }
            else{
                //Sign out
                AuthUI.getInstance()
                        .signOut(this)
                        .addOnCompleteListener(task -> Toast.makeText(this, "Signed out",
                                Toast.LENGTH_LONG).show());
                loggedIn = false;
                btnLogin.setText(getString(R.string.login));
                btnShowMap.setAlpha(0.4f);
            }
        });
        btnShowMap.setOnClickListener(v -> {
            if (loggedIn){
                Intent intent = new Intent(context, MapActivity.class);
                startActivity(intent);
            }
            else
                Toast.makeText(this,"Please login first", Toast.LENGTH_SHORT).show();

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                // Successfully signed in
                loggedIn = true;
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null) {
                    Toast.makeText(this, "Welcome "+user.getDisplayName(), Toast.LENGTH_SHORT).show();
                }
                // ...
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise checkLocationServices
                // response.getError().getErrorCode() and handle the error.
                // ...
            }
        }
    }

    @Override
    protected void onResume() {
        System.out.println("OnResume ------------------------------------------------------------");
        if(loggedIn){
            btnLogin.setText(getString(R.string.sign_out));
            btnShowMap.setAlpha(1.0f);
        }

        else{
            btnLogin.setText(getString(R.string.login));
            btnShowMap.setAlpha(0.4f);
        }
        super.onResume();

    }
}
