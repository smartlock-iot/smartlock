package hr.ferit.student.smartlock;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import androidx.core.app.ActivityCompat;

class LocationHelper {

    private GoogleMap mMap;
    private Activity activity;
    private Context context;
    private String[] reqPermissions;
    private int MY_LOCATION_REQ_CODE = 12;
    public static int GPS_REQ_CODE = 14;
    private FusedLocationProviderClient mFusedLocationClient;

    LocationHelper(Activity activity, GoogleMap mMap) {
        this.mMap = mMap;
        this.activity = activity;
        this.context = activity.getApplicationContext();
        reqPermissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
    }

    LocationHelper(Activity activity) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        reqPermissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
    }

    void checkGPS(Context context){
        LocationManager lm = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }
        catch(Exception ignored) {}

        if(!gps_enabled) {
            // notify user
            new AlertDialog.Builder(context)
                    .setTitle("Enable location")
                    .setMessage(context.getResources().getString(R.string.gps_enable_msg))
                    .setPositiveButton("OK", (dialog, which) -> {
                        context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    })
                    .setNegativeButton("CANCEL", (dialog, which) -> {
                        dialog.dismiss();
                    })
                    .show();
        }
    }

    void checkGPSForService(Activity activity, Context context){
        LocationManager lm = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }
        catch(Exception ignored) {}

        if(!gps_enabled) {
            // notify user
            new AlertDialog.Builder(context)
                    .setTitle("Enable location")
                    .setMessage(context.getResources().getString(R.string.gps_enable_msg))
                    .setPositiveButton("OK", (dialog, which) -> {
                        activity.startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), GPS_REQ_CODE);
                    })
                    .setNegativeButton("CANCEL", (dialog, which) -> {
                        dialog.dismiss();
                    })
                    .show();
        }
    }

    boolean isGPSEnabled(){
        LocationManager lm = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }
        catch(Exception ignored) {}
        return gps_enabled;
    }

    void checkPermissions(){
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
               this.activity.requestPermissions(reqPermissions, MY_LOCATION_REQ_CODE);
            }
        }
    }

    void onRequestPermissionsResult(int requestCode, int[] grantResults){
        if (requestCode == MY_LOCATION_REQ_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);
                }
            }
            else{
                Toast.makeText(context, context.getResources().getString(R.string.loc_permission_denied), Toast.LENGTH_LONG).show();
            }
        }
    }

    void updateMarkerLocation(MarkerOptions marker, boolean updateCamera){
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient.getLastLocation().addOnSuccessListener(location -> {
                if(location!=null){
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    marker.position(latLng);
                    mMap.clear();
                    mMap.addMarker(marker);
                    if (updateCamera) mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
                }
            });
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                this.activity.requestPermissions(reqPermissions, MY_LOCATION_REQ_CODE);
            }
        }
    }
}
