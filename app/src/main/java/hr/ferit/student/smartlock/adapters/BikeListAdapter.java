package hr.ferit.student.smartlock.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.firebase.database.ServerValue;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import hr.ferit.student.smartlock.R;
import hr.ferit.student.smartlock.model.Bike;

public class BikeListAdapter extends ArrayAdapter<Bike>  {

    private Context context;
    private ArrayList<Bike> bikesList;

    public BikeListAdapter(@NonNull Context context, ArrayList<Bike> bike) {
        super(context, R.layout.list_row, bike);
        this.context = context;
        this.bikesList = bike;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        View rowView = LayoutInflater.from(context).inflate(R.layout.list_row, parent, false);

        Bike currentBike = bikesList.get(position);

        TextView lvBikeName = rowView.findViewById(R.id.lvBikeName);
        TextView lvTimestamp = rowView.findViewById(R.id.lvTimestamp);
        TextView lvDistance = rowView.findViewById(R.id.lvDistance);
        TextView lvCalories = rowView.findViewById(R.id.lvCalories);
        lvBikeName.setText(currentBike.getName());
        lvTimestamp.setText(currentBike.getTimestamp());

        lvDistance.setText(getDistance(currentBike.getDistance()));
        lvCalories.setText(getCalories(currentBike));

        return rowView;
    }

    private String getCalories(Bike bike) {
        long calories;
        long distance = bike.getDistance();
        long timeDiff = (System.currentTimeMillis() - bike.timestamp)/1000; //seconds

        double avgSpeed = (double)distance/timeDiff;

        calories = (long)((2.24*avgSpeed*200*(0.006)+0.009*Math.pow(2.24*avgSpeed,3))*9*timeDiff/3600);

        return "Calories: "+String.valueOf(calories)+" kcal";
    }

    private String getDistance(long distance){
        return "Distance: "+String.valueOf(distance)+" m";
    }

}
