package hr.ferit.student.smartlock.model;


import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Bike {

    private String id;
    private String name;
    private String user;
    private Location location;
    public long timestamp;
    private long distance;

    public Bike(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public LatLng getLocation() {
        return new LatLng(this.location.getLatitude(), this.location.getLongitude());
    }

    public Location getMyLocation(){
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimestamp() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy   HH:mm");
        return sdf.format(new Date(this.timestamp));
    }

    public long getDistance() {
        return distance;
    }

    public void setDistance(long distance) {
        this.distance = distance;
    }

}
