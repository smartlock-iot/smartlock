package hr.ferit.student.smartlock;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Map;

import hr.ferit.student.smartlock.model.Bike;
import hr.ferit.student.smartlock.model.Location;

import static hr.ferit.student.smartlock.LocationHelper.GPS_REQ_CODE;
import static hr.ferit.student.smartlock.WelcomeActivity.loggedIn;

public class MapActivity extends AppCompatActivity implements
        OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        NavigationView.OnNavigationItemSelectedListener,
        GoogleMap.OnMyLocationButtonClickListener{

    private GoogleMap mMap;
    LocationHelper locationHelper;
    private final int MAP_UPDATE_DURATION = 400;
    private ArrayList<Bike> bikes = new ArrayList<>();
    Context context;
    BottomSheetDialog bottomSheetDialog;
    TextView tvUserName;
    String[] reqPermissions;
    FirebaseDatabase database;
    DatabaseReference myRef;
    private TextView tvBikeNumber;
    private TextView tvLatitude;
    private TextView tvLongitude;
    private Button btnUnlock;
    DrawerLayout drawer;
    public static boolean userUnlockedFlag = false;
    private String bikeId;
    private AlertDialog distanceDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        context = this;

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        reqPermissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};

        setupToolbar();
        setupDrawer();
        setupBottomSheetDialog();
        database = FirebaseDatabase.getInstance();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMarkerClickListener(this);
        mMap.setOnMyLocationButtonClickListener(this);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(45.53, 18.7)));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(14));

        locationHelper = new LocationHelper(MapActivity.this, mMap);
        locationHelper.checkPermissions();

        getBikes();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        locationHelper.onRequestPermissionsResult(requestCode, grantResults);
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(marker.getPosition())
                .zoom(16)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), MAP_UPDATE_DURATION, null);
        new Handler().postDelayed(() -> showBottomSheetDialog(marker), MAP_UPDATE_DURATION);

        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_return) {
            Intent intent = new Intent(this, MyBikesActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_signout) {

            signout();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private Bitmap getBitmap() {
        Drawable drawable = getResources().getDrawable(R.drawable.ic_bike_marker);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private void getBikes() {
        myRef = database.getReference("bikes");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                bikes.clear();
                Log.e("Count ", "" + dataSnapshot.getChildrenCount());
                for (DataSnapshot bikeSnapshot : dataSnapshot.getChildren()) {
                    Bike bike = bikeSnapshot.getValue(Bike.class);
                    if (bike != null)
                        bike.setId(bikeSnapshot.getKey());
                    bikes.add(bike);
                    Log.e("bikes", "count" + bikes.size());
                }
                setBikes();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("Firebase", "loadPost:onCancelled", databaseError.toException());
            }
        });
    }

    private void setBikes() {
        mMap.clear();
        for (Bike bike : bikes) {
            if (bike.getUser() == null) {
                MarkerOptions marker = new MarkerOptions()
                        .position(bike.getLocation())
                        .title(bike.getName())
                        .icon(BitmapDescriptorFactory.fromBitmap(getBitmap()));
                mMap.addMarker(marker).setTag(bike.getId());
            }
        }
        mMap.animateCamera(CameraUpdateFactory.newLatLng(bikes.get(0).getLocation()));
    }

    private void setupToolbar() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerLayout = navigationView.getHeaderView(0);
        tvUserName = headerLayout.findViewById(R.id.tvUserName);
        tvUserName.setText(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
    }

    private void setupDrawer(){
        drawer = findViewById(R.id.drawer_layout);
        NavigationView nView = findViewById(R.id.nav_view);
        Menu menu = nView.getMenu();
        MenuItem menuItem = menu.findItem(R.id.nav_return);
        TextView tvBikeCount = (TextView) menuItem.getActionView().findViewById(R.id.tvBikeCount);

            myRef = FirebaseDatabase.getInstance().getReference("users").child(FirebaseAuth.getInstance().getUid());
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    tvBikeCount.setText(String.valueOf(dataSnapshot.getChildrenCount()));
                    if(dataSnapshot.getChildrenCount() != 0)
                        tvBikeCount.setBackground(getResources().getDrawable(R.drawable.custom_tv_bg_accent));
                    else
                        tvBikeCount.setBackground(getResources().getDrawable(R.drawable.custom_tv_bg));
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    // Getting Post failed, log a message
                    Log.w("Firebase", "loadPost:onCancelled", databaseError.toException());
                }
            });

    }

    private void showBottomSheetDialog(Marker marker) {
        tvBikeNumber.setText(marker.getTitle());
        DecimalFormat df = new DecimalFormat("#.####");
        tvLatitude.setText(String.valueOf(df.format(marker.getPosition().latitude)));
        tvLongitude.setText(String.valueOf(df.format(marker.getPosition().longitude)));

        btnUnlock.setOnClickListener(v -> {
            Intent bluetoothIntent = new Intent(MapActivity.this, UnlockActivity.class);
            bikeId = (String)marker.getTag();
            bluetoothIntent.putExtra(getString(R.string.bike_id_intent), bikeId);
            bluetoothIntent.putExtra(getString(R.string.bike_name_intent), marker.getTitle());
            startActivity(bluetoothIntent);
            bottomSheetDialog.dismiss();

        });
        bottomSheetDialog.show();
    }

    private void setupBottomSheetDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.bottom_sheet_dialog, null);
        tvBikeNumber = view.findViewById(R.id.tvBikeName);
        tvLatitude = view.findViewById(R.id.tvLatitude);
        tvLongitude = view.findViewById(R.id.tvLongitude);
        btnUnlock = view.findViewById(R.id.btnSheetUnlock);

        bottomSheetDialog = new BottomSheetDialog(context);
        bottomSheetDialog.setContentView(view);
    }

    private void signout() {
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(task -> Toast.makeText(this, "Signed out",
                        Toast.LENGTH_LONG).show());
        loggedIn = false;
        this.finish();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        locationHelper.checkGPS(this);
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(bottomSheetDialog !=null && bottomSheetDialog.isShowing())
            bottomSheetDialog.dismiss();
        if(distanceDialog!=null && distanceDialog.isShowing())
            distanceDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("ON RESUME MAP ACT");
        if(userUnlockedFlag){
            userUnlockedFlag = false;
            showDistanceTrackingDialog();
        }
        if(getIntent().getBooleanExtra(getString(R.string.nofitication_ssd), false)){
            getIntent().removeExtra(getString(R.string.nofitication_ssd));
            showDistanceStopDialog();
        }
    }

    private void showDistanceStopDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.distance_dialog, null);
        TextView msg = view.findViewById(R.id.msg);
        msg.setText(getString(R.string.distance_stop_msg));
        dialogBuilder.setView(view);

        distanceDialog = dialogBuilder
                .setPositiveButton("YES, STOP IT", (dialog, which) -> {
                    stopService();
                    dialog.dismiss();
                })
                .setNegativeButton("CANCEL", (dialog, which) -> {
                    dialog.dismiss();
                })
                .create();
        distanceDialog.show();
    }

    private void showDistanceTrackingDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.distance_dialog, null);
        dialogBuilder.setView(view);
        distanceDialog = dialogBuilder
                .setPositiveButton("YES, ENABLE IT", (dialog, which) -> {
                    startService();
                })
                .setNegativeButton("NO, THANKS", (dialog, which) -> {
                    try {
                        stopService();
                    }
                    catch (Exception ignored){}
                    dialog.dismiss();
                })
                .create();
        distanceDialog.show();
    }

    public void startService(){
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if(locationHelper.isGPSEnabled()){
                Intent serviceIntent = new Intent(this, LocationService.class);
                serviceIntent.putExtra(getString(R.string.service_user), FirebaseAuth.getInstance().getUid());
                serviceIntent.putExtra(getString(R.string.service_bike), bikeId);
                startService(serviceIntent);
                distanceDialog.dismiss();
            }
            else{
                locationHelper.checkGPSForService(MapActivity.this, this);
            }
        } else {
            distanceDialog.dismiss();
            Toast.makeText(this, "Tracking your distance can't be started becouse of location permission issue.",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GPS_REQ_CODE){
            if(locationHelper.isGPSEnabled()){
                System.out.println("Starting distance tracking...");
                startService();
            }
            else{
                Toast.makeText(this, "Distance tracking stopped - GPS not enabled", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void stopService(){
        Intent serviceIntent = new Intent(this, LocationService.class);
        stopService(serviceIntent);
    }
}
