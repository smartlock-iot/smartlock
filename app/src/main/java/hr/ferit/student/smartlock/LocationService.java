package hr.ferit.student.smartlock;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.IBinder;
import android.os.Looper;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

public class LocationService extends Service {

    NotificationHelper notificationHelper;
    FusedLocationProviderClient fusedLocationProviderClient;
    LocationCallback locationCallback;
    private LocationRequest locationRequest;
    long distance;
    Location oldLocation;
    String userId;
    String bikeId;

    @Override
    public void onCreate() {
        super.onCreate();
        notificationHelper = new NotificationHelper(this);
        buildLocationRequest();
        buildLocationCallback();
        requestLocationUpdates();
        distance = 0;
    }

    private void requestLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        userId = intent.getStringExtra(getString(R.string.service_user));
        bikeId = intent.getStringExtra(getString(R.string.service_bike));
        startForeground(1, notificationHelper.createNotification("Tracking your biking..."));
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void buildLocationRequest(){
        locationRequest = new LocationRequest()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(5000)
                .setFastestInterval(3000)
                .setSmallestDisplacement(5);
    }

    private void buildLocationCallback(){
        locationCallback = new LocationCallback()
        {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (oldLocation==null)
                    oldLocation = locationResult.getLastLocation();
                else{
                    distance = distance + (long)oldLocation.distanceTo(locationResult.getLastLocation());
                    oldLocation = locationResult.getLastLocation();
                }
                writeToDatabase();
            }
        };
    }

    private void writeToDatabase() {
        DatabaseReference dbReference = FirebaseDatabase.getInstance()
                .getReference("users").child(userId).child("bikes").child(bikeId);
        dbReference.child("distance").setValue(distance);
    }
}
