package hr.ferit.student.smartlock;

import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import hr.ferit.student.smartlock.adapters.BTListAdapter;
import me.aflak.bluetooth.Bluetooth;
import me.aflak.bluetooth.BluetoothCallback;
import me.aflak.bluetooth.DeviceCallback;
import me.aflak.bluetooth.DiscoveryCallback;

public class LockCheckActivity extends AppCompatActivity {

    Bluetooth bluetooth;
    Context context;
    LottieAnimationView scanAnimation, lockAnimation;
    String bikeId;
    String bikeName;
    String password;
    BTListAdapter paired, available;
    Button btnCheck, btnDone;
    boolean scanning;
    private AlertDialog lockDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unlock);

        bikeName = getIntent().getStringExtra(getString(R.string.bike_name_intent));

        context = this;
        bluetooth = new Bluetooth(context);

        setupViews();
    }

    private void setupViews() {
        scanAnimation = findViewById(R.id.animation_view);
        ListView lvPaired = findViewById(R.id.paired_devices);
        ListView lvBTDevices = findViewById(R.id.available_devices);

        scanAnimation.setOnClickListener(v -> {
            if (scanning && bluetooth.isEnabled()){
                stopScanAnimation();
                bluetooth.stopScanning();
            }
            else{
                if(bluetooth.isEnabled()){
                    paired.clear();
                    available.clear();
                    bluetooth.startScanning();
                    startScanAnimation();
                    scanning = true;
                }
                else
                    bluetooth.showEnableDialog(this);
            }
        });

        paired = new BTListAdapter(this, new ArrayList<>());
        available = new BTListAdapter(this, new ArrayList<>());

        lvPaired.setAdapter(paired);
        lvPaired.setOnItemClickListener((parent, view, position, id) -> {
            System.out.println(paired.getItem(position));
            if(scanning){
                stopScanAnimation();
                bluetooth.stopScanning();
            }
            showLockDialog(paired.getItem(position));
        });

        lvBTDevices.setAdapter(available);
        lvBTDevices.setOnItemClickListener((parent, view, position, id) -> {
            System.out.println(available.getItem(position));
            bluetooth.pair(available.getItem(position));
            Toast.makeText(context,"Pairing...", Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        bluetooth.onStart();
        if (bluetooth.isEnabled()){
            startScan();
        }
        else{
            stopScanAnimation();
            bluetooth.showEnableDialog(this);
            bluetooth.setBluetoothCallback(new BluetoothCallback() {
                @Override
                public void onBluetoothTurningOn() {
                    System.out.println("BT TURNING ON");
                }

                @Override
                public void onBluetoothOn() {
                    startScan();
                }

                @Override
                public void onBluetoothTurningOff() {
                }

                @Override
                public void onBluetoothOff() {
                }

                @Override
                public void onUserDeniedActivation() {
                    System.out.println("BT DENY");
                    Toast.makeText(context,
                            "Please turn on Bluetooth on your device as it is necessary for our lock check.",
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (bluetooth.isConnected())
            bluetooth.disconnect();
        bluetooth.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("ACTIVITY RESULT: "+resultCode);
        bluetooth.onActivityResult(requestCode, resultCode);
    }

    private void startScan() {
        scanning = true;
        startScanAnimation();
        bluetooth.setDiscoveryCallback(new DiscoveryCallback() {
            @Override
            public void onDiscoveryStarted() {
                Toast.makeText(context,"Discovery started", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDiscoveryFinished() {
                stopScanAnimation();
                if(paired.isEmpty() && available.isEmpty()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("No SmartLocks found")
                            .setMessage("Check if you are near your SmartLock device.")
                            .setPositiveButton("OK", (dialog, which) -> {
                                dialog.dismiss();
                            })
                            .setIcon(R.drawable.ic_smartlock_logo_blue);
                    builder.show();
                }
                else
                    Toast.makeText(context,"Discovery finished", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDeviceFound(BluetoothDevice device) {
                if(device.getName()!=null && device.getName().equals(bikeName)) {
                    if(bluetooth.getPairedDevices().contains(device)){
                        paired.add(device);
                    }
                    else available.add(device);
                }
            }

            @Override
            public void onDevicePaired(BluetoothDevice device) {
                System.out.println("DEVICE PAIRED: "+device.getName());
                paired.add(device);
                available.remove(device);
                Toast.makeText(context,"Pairing successful", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDeviceUnpaired(BluetoothDevice device) {

            }

            @Override
            public void onError(String message) {

            }
        });
        bluetooth.startScanning();
    }

    private void connect(BluetoothDevice device){
        bluetooth.setDeviceCallback(new DeviceCallback() {
            @Override
            public void onDeviceConnected(BluetoothDevice device) {
                System.out.println("CONNECTED!");
                bluetooth.send(getString(R.string.bt_msg_lock_check));
            }

            @Override
            public void onDeviceDisconnected(BluetoothDevice device, String message) {

            }

            @Override
            public void onMessage(String message) {
                System.out.println("BT MSG: "+message);
                if(message.equals("LOCKED")){
                    playAnimation(R.raw.lock);
                    System.out.println("SmartLock is LOCKED!!!!!!!!!");
                }
                if(message.equals("TIMEOUT")){
                    stopAnimation();
                    System.out.println("SmartLock is not locked. You can try again...");
                }
            }
            @Override
            public void onError(String message) {

            }

            @Override
            public void onConnectError(BluetoothDevice device, String message) {

            }
        });
        bluetooth.connectToDevice(device);
    }

    private void playAnimation(int animation){
        new Handler(Looper.getMainLooper()).post(() -> {
            lockAnimation.setAnimation(animation);
            lockAnimation.loop(false);
            lockAnimation.setMaxProgress(0.9f);
            lockAnimation.playAnimation();
            btnCheck.setVisibility(View.GONE);
            btnDone.setVisibility(View.VISIBLE);
        });
    }

    private void stopAnimation(){
        lockAnimation.loop(false);
        lockAnimation.setProgress(1f);
        btnCheck.setText("TRY AGAIN");
        if (bluetooth.isConnected())
            bluetooth.disconnect();
        runOnUiThread(() -> {
            Toast.makeText(context, "Timeout: App couldn't detect that you locked your SmartLock. Try again.",
                    Toast.LENGTH_LONG).show();
        });
    }

    private void stopScanAnimation(){
        scanAnimation.loop(false);
        scanAnimation.setProgress(1.0f);
        scanAnimation.setAlpha(0.4f);
        scanning=false;
    }

    private void startScanAnimation(){
        scanAnimation.setAlpha(1.0f);
        scanAnimation.loop(true);
        scanAnimation.playAnimation();
    }

    private void showLockDialog(BluetoothDevice device) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.lock_dialog, null);
        dialogBuilder.setView(view);

        lockAnimation = view.findViewById(R.id.animation_view);
        btnCheck = view.findViewById(R.id.btnLock);
        btnDone = view.findViewById(R.id.btnDone);

        lockDialog = dialogBuilder.create();
        lockDialog.setOnDismissListener(dialog -> {
            if(bluetooth.isConnected())
                bluetooth.disconnect();
        });
        btnCheck.setOnClickListener(v->{
            lockAnimation.loop(true);
            lockAnimation.playAnimation();
            connect(device);
        });

        btnDone.setOnClickListener(v -> {
            lockDialog.dismiss();
            setResult(RESULT_OK);
            this.finish();
        });

        lockDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(lockDialog!=null && lockDialog.isShowing())
            lockDialog.dismiss();
    }

}
