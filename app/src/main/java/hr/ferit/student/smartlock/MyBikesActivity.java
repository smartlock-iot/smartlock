package hr.ferit.student.smartlock;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import hr.ferit.student.smartlock.adapters.BikeListAdapter;
import hr.ferit.student.smartlock.model.Bike;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Objects;

import static hr.ferit.student.smartlock.LocationHelper.GPS_REQ_CODE;

public class MyBikesActivity extends AppCompatActivity {

    private ListView listView;
    private BikeListAdapter mAdapter;

    FirebaseDatabase database;
    DatabaseReference myRef;
    Context context;
    ArrayList<Bike> bikesList;
    BottomSheetDialog bottomSheetDialog;
    TextView tvBSBikeName;
    LinearLayout optionReUnlock;
    LinearLayout optionReturn;
    LinearLayout optionTracking;
    private int RC_LOCK_CHECK = 13;
    Bike clickedBike;
    private AlertDialog distanceDialog;
    private LocationHelper locationHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mybikes);

        context = this;
        database = FirebaseDatabase.getInstance();
        locationHelper = new LocationHelper(this);

        setupToolbar();
        setupListView();
        setupBSDialog();
    }

    private void setupToolbar(){
        Toolbar returnToolbar = findViewById(R.id.returnToolbar);
        setSupportActionBar(returnToolbar);

        TextView btnToolbarBack = findViewById(R.id.btnToolbarBack);

        btnToolbarBack.setOnClickListener(v -> this.finish());
    }

    private void setupListView(){
        listView = findViewById(R.id.lvBikes);
        bikesList = new ArrayList<>();
        mAdapter = new BikeListAdapter(context, bikesList);
        listView.setAdapter(mAdapter);
    }

    private void getUserBikes() {
        myRef = database.getReference("users").child(FirebaseAuth.getInstance().getUid()).child("bikes");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.e("Count " ,""+dataSnapshot.getChildrenCount());
                for(DataSnapshot bikeSnapshot : dataSnapshot.getChildren()) {
                    getBike(bikeSnapshot.getKey(),
                            (long)bikeSnapshot.child("timestamp").getValue(),
                            (long)bikeSnapshot.child("distance").getValue());
                }
                listView.setOnItemClickListener((parent, view, position, id) -> {
                    clickedBike = bikesList.get(position);
                    showBSDialog();
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("Firebase", "loadPost:onCancelled", databaseError.toException());
            }
        });
    }

    private void setupBSDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.options_bs_dialog, null);
        tvBSBikeName = view.findViewById(R.id.tvBikeName);
        optionReturn = view.findViewById(R.id.option_return);
        optionReUnlock = view.findViewById(R.id.option_re_unlock);
        optionTracking = view.findViewById(R.id.option_tracking);

        bottomSheetDialog = new BottomSheetDialog(context);
        bottomSheetDialog.setContentView(view);
    }

    private void showBSDialog() {
        tvBSBikeName.setText(clickedBike.getName());

        optionReturn.setOnClickListener(v -> {
            new AlertDialog.Builder(context)
                    .setTitle("Lock check")
                    .setMessage(getString(R.string.msg_lock_check))
                    .setPositiveButton("OK", (dialog, which) -> {
                        Intent lockCheckIntent = new Intent(MyBikesActivity.this, LockCheckActivity.class);
                        lockCheckIntent.putExtra(getString(R.string.bike_name_intent), clickedBike.getName());
                        //startActivity(lockCheckIntent);
                        startActivityForResult(lockCheckIntent, RC_LOCK_CHECK);
                    })
                    .setNegativeButton("CANCEL", (dialog, which) -> dialog.dismiss())
                    .show();
            bottomSheetDialog.dismiss();
        });

        optionReUnlock.setOnClickListener(v -> {
            Intent reUnlockIntent = new Intent(MyBikesActivity.this, UnlockActivity.class);
            reUnlockIntent.putExtra(getString(R.string.bike_id_intent), Objects.requireNonNull(clickedBike.getId()));
            reUnlockIntent.putExtra(getString(R.string.bike_name_intent), clickedBike.getName());
            reUnlockIntent.putExtra(getString(R.string.reunlock_intent_flag), true);
            startActivity(reUnlockIntent);
            bottomSheetDialog.dismiss();
        });

        optionTracking.setOnClickListener(v->{
            showDistanceTrackingDialog();
            bottomSheetDialog.dismiss();
        });

        bottomSheetDialog.show();
    }

    private void getBike(String bikeId, long timestamp, long distance){
        myRef = database.getReference("bikes").child(bikeId);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Bike bike = dataSnapshot.getValue(Bike.class);
                if (bike != null){
                    bike.setId(bikeId);
                    bike.setTimestamp(timestamp);
                    bike.setDistance(distance);
                }
                mAdapter.add(bike);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("Firebase", "loadPost:onCancelled", databaseError.toException());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdapter.clear();
        getUserBikes();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==RC_LOCK_CHECK){
            if(resultCode==RESULT_OK){
                Intent intent = new Intent(MyBikesActivity.this, ReturnMapActivity.class);
                intent.putExtra(getString(R.string.bike_id_intent), clickedBike.getId());
                intent.putExtra(getString(R.string.bike_location_intent), clickedBike.getLocation());
                startActivity(intent);
            }
        }
        if(requestCode == GPS_REQ_CODE){
            if(locationHelper.isGPSEnabled()){
                System.out.println("Starting distance tracking...");
                startService();
            }
            else{
                Toast.makeText(this, "Distance tracking stopped - GPS not enabled", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void showDistanceTrackingDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.distance_dialog, null);
        dialogBuilder.setView(view);
        distanceDialog = dialogBuilder
                .setPositiveButton("YES, ENABLE IT", (dialog, which) -> {
                    startService();
                })
                .setNegativeButton("NO, THANKS", (dialog, which) -> {
                    try {
                        stopService();
                    }
                    catch (Exception ignored){}
                    dialog.dismiss();
                })
                .create();
        distanceDialog.show();
    }

    public void startService(){
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if(locationHelper.isGPSEnabled()){
                Intent serviceIntent = new Intent(this, LocationService.class);
                serviceIntent.putExtra(getString(R.string.service_user), FirebaseAuth.getInstance().getUid());
                serviceIntent.putExtra(getString(R.string.service_bike), clickedBike.getId());
                startService(serviceIntent);
                distanceDialog.dismiss();
            }
            else{
                locationHelper.checkGPSForService(MyBikesActivity.this, this);
            }
        } else {
            distanceDialog.dismiss();
            Toast.makeText(this, "Tracking your distance can't be started becouse of location permission issue.",
                    Toast.LENGTH_LONG).show();
        }
    }

    public void stopService(){
        Intent serviceIntent = new Intent(this, LocationService.class);
        stopService(serviceIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(distanceDialog!=null && distanceDialog.isShowing())
            distanceDialog.dismiss();
        if(bottomSheetDialog !=null && bottomSheetDialog.isShowing())
            bottomSheetDialog.dismiss();
    }
}
