package hr.ferit.student.smartlock.adapters;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import hr.ferit.student.smartlock.R;

public class BTListAdapter extends ArrayAdapter<BluetoothDevice> {

    private Context mContext;
    public List<BluetoothDevice> devices;

    public BTListAdapter(@NonNull Context context, ArrayList<BluetoothDevice> list) {
        super(context, 0 , list);
        mContext = context;
        devices = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.bt_device_row,parent,false);

        BluetoothDevice device = devices.get(position);

        TextView name = listItem.findViewById(R.id.name);
        TextView address = listItem.findViewById(R.id.address);

        name.setText(device.getName());
        address.setText(device.getAddress());

        return listItem;
    }
}