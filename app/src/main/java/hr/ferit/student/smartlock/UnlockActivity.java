package hr.ferit.student.smartlock;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.airbnb.lottie.LottieAnimationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import hr.ferit.student.smartlock.adapters.BTListAdapter;
import me.aflak.bluetooth.Bluetooth;
import me.aflak.bluetooth.BluetoothCallback;
import me.aflak.bluetooth.DeviceCallback;
import me.aflak.bluetooth.DiscoveryCallback;

import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import static hr.ferit.student.smartlock.MapActivity.userUnlockedFlag;

public class UnlockActivity extends AppCompatActivity {

    Bluetooth bluetooth;
    Context context;
    LottieAnimationView scanAnimation, unlockAnimation;
    String bikeId;
    String bikeName;
    String password;
    BTListAdapter paired, available;
    Button btnUnlock, btnDone;
    boolean scanning;
    private boolean reUnlock = false;
    CountDownTimer counter;
    private AlertDialog unlockDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unlock);

        bikeId = getIntent().getStringExtra(getString(R.string.bike_id_intent));
        bikeName = getIntent().getStringExtra(getString(R.string.bike_name_intent));
        reUnlock = getIntent().getBooleanExtra(getString(R.string.reunlock_intent_flag), false);
        getPassword();

        context = this;
        bluetooth = new Bluetooth(context);

        setupViews();
    }

    private void setupViews() {
        scanAnimation = findViewById(R.id.animation_view);
        ListView lvPaired = findViewById(R.id.paired_devices);
        ListView lvBTDevices = findViewById(R.id.available_devices);

        scanAnimation.setOnClickListener(v -> {
            if (scanning && bluetooth.isEnabled()){
                stopScanAnimation();
                bluetooth.stopScanning();
            }
            else{
                if(bluetooth.isEnabled()){
                    paired.clear();
                    available.clear();
                    bluetooth.startScanning();
                    startScanAnimation();
                    scanning = true;
                }
                else
                    bluetooth.showEnableDialog(this);
            }
        });

        paired = new BTListAdapter(this, new ArrayList<>());
        available = new BTListAdapter(this, new ArrayList<>());

        lvPaired.setAdapter(paired);
        lvPaired.setOnItemClickListener((parent, view, position, id) -> {
            System.out.println(paired.getItem(position));
            if(scanning){
                stopScanAnimation();
                bluetooth.stopScanning();
            }
            showUnlockDialog(paired.getItem(position));
        });

        lvBTDevices.setAdapter(available);
        lvBTDevices.setOnItemClickListener((parent, view, position, id) -> {
            System.out.println(available.getItem(position));
            bluetooth.pair(available.getItem(position));
            Toast.makeText(context,"Pairing...", Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        bluetooth.onStart();
        setupCounter();
        if (bluetooth.isEnabled()){
            startScan();
        }
        else{
            stopScanAnimation();
            bluetooth.showEnableDialog(this);
            bluetooth.setBluetoothCallback(new BluetoothCallback() {
                @Override
                public void onBluetoothTurningOn() {
                    System.out.println("BT TURNING ON");
                }

                @Override
                public void onBluetoothOn() {
                    startScan();
                }

                @Override
                public void onBluetoothTurningOff() {
                }

                @Override
                public void onBluetoothOff() {
                }

                @Override
                public void onUserDeniedActivation() {
                    System.out.println("BT DENY");
                    Toast.makeText(context,
                            "Please turn on Bluetooth on your device as it is necessary for unlocking the SmartLock",
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void setupCounter() {
        counter = new CountDownTimer(10000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                onUnlockError();
            }
        };
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (bluetooth.isConnected())
            bluetooth.disconnect();
        bluetooth.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("ACTIVITY RESULT: "+resultCode);
        bluetooth.onActivityResult(requestCode, resultCode);
    }

    private void getPassword() {
        DatabaseReference dbReference = FirebaseDatabase.getInstance()
                .getReference("bikes").child(bikeId);
        dbReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                password = (String)dataSnapshot.child("password").getValue();
                System.out.println(password);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("Firebase", "loadPost:onCancelled", databaseError.toException());
            }
        });
    }

    private void startScan() {
        scanning = true;
        startScanAnimation();
        bluetooth.setDiscoveryCallback(new DiscoveryCallback() {
            @Override
            public void onDiscoveryStarted() {
                Toast.makeText(context,"Discovery started", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDiscoveryFinished() {
                stopScanAnimation();
                if(paired.isEmpty() && available.isEmpty()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("No SmartLocks found")
                            .setMessage(R.string.no_devices)
                            .setPositiveButton("OK", (dialog, which) -> dialog.dismiss())
                            .setIcon(R.drawable.ic_smartlock_logo_blue);
                    builder.show();
                }
                else
                    Toast.makeText(context,"Discovery finished", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDeviceFound(BluetoothDevice device) {
                if(device.getName()!=null && device.getName().equals(bikeName)) {
                    if(bluetooth.getPairedDevices().contains(device)){
                        paired.add(device);
                    }
                    else available.add(device);
                }
            }

            @Override
            public void onDevicePaired(BluetoothDevice device) {
                System.out.println("DEVICE PAIRED: "+device.getName());
                paired.add(device);
                available.remove(device);
                Toast.makeText(context,"Pairing successful", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDeviceUnpaired(BluetoothDevice device) {

            }

            @Override
            public void onError(String message) {

            }
        });
        bluetooth.startScanning();
    }

    private void connect(BluetoothDevice device){
        counter.start();
        bluetooth.setDeviceCallback(new DeviceCallback() {
            @Override
            public void onDeviceConnected(BluetoothDevice device) {
                System.out.println("CONNECTED!");
                bluetooth.send(getString(R.string.msg_client_ready));
            }

            @Override
            public void onDeviceDisconnected(BluetoothDevice device, String message) {

            }

            @Override
            public void onMessage(String message) {
                System.out.println("BT MSG: "+message);
                if(message.equals(getString(R.string.msg_smartlock_ready)))
                    bluetooth.send(getHash(password));
                if(message.equals(getString(R.string.unlock_result_success))){
                    counter.cancel();
                    onUnlock();
                }
                if(message.equals(getString(R.string.unlock_result_error))){
                    onUnlockError();
                }
            }
            @Override
            public void onError(String message) {

            }

            @Override
            public void onConnectError(BluetoothDevice device, String message) {

            }
        });
        bluetooth.connectToDevice(device);
    }

    private void onUnlockError() {
        unlockAnimation.loop(false);
        unlockAnimation.setProgress(1f);
        btnUnlock.setText(getString(R.string.try_again));
        if (bluetooth.isConnected())
            bluetooth.disconnect();
        runOnUiThread(() -> Toast.makeText(context, getString(R.string.unlock_error_msg),
                Toast.LENGTH_LONG).show());
    }

    private void onUnlock() {
        playAnimation(R.raw.unlock);
        System.out.println("REUNLOCK: "+reUnlock);
        if (!reUnlock)
            updateUserBikes();
    }

    private void updateUserBikes() {
        DatabaseReference dbReference = FirebaseDatabase.getInstance()
                .getReference("users").child(FirebaseAuth.getInstance().getUid())
                .child("bikes").child(bikeId);
        dbReference.child("distance").setValue(0.0);
        dbReference.child("timestamp").setValue(ServerValue.TIMESTAMP);

        updateBikes();
    }

    private void updateBikes(){
        DatabaseReference dbRef = FirebaseDatabase.getInstance()
                .getReference("bikes").child(bikeId);
        dbRef.child("user").setValue(FirebaseAuth.getInstance().getUid());
    }

    private void playAnimation(int animation){
        new Handler(Looper.getMainLooper()).post(() -> {
            unlockAnimation.setAnimation(animation);
            unlockAnimation.loop(false);
            unlockAnimation.setMaxProgress(0.9f);
            unlockAnimation.playAnimation();
            btnUnlock.setVisibility(View.GONE);
            btnDone.setVisibility(View.VISIBLE);
        });
    }

    private void stopScanAnimation(){
        scanAnimation.loop(false);
        scanAnimation.setProgress(1.0f);
        scanAnimation.setAlpha(0.4f);
        scanning=false;
    }

    private void startScanAnimation(){
        scanAnimation.setAlpha(1.0f);
        scanAnimation.loop(true);
        scanAnimation.playAnimation();
    }

    public static String getHash(String password) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(password.getBytes(StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            for (byte anArray : array) {
                sb.append(Integer.toHexString((anArray & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        }
        catch (java.security.NoSuchAlgorithmException ignored) {}
        return null;
    }

    private void showUnlockDialog(BluetoothDevice device) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.unlock_dialog, null);
        dialogBuilder.setView(view);

        unlockAnimation = view.findViewById(R.id.animation_view);
        btnUnlock = view.findViewById(R.id.btnUnlock);
        btnDone = view.findViewById(R.id.btnDone);

        unlockDialog = dialogBuilder.create();
        unlockDialog.setOnDismissListener(dialog -> {
            if(bluetooth.isConnected())
                bluetooth.disconnect();
        });

        btnUnlock.setOnClickListener(v->{
            unlockAnimation.loop(true);
            unlockAnimation.playAnimation();
            connect(device);
        });

        btnDone.setOnClickListener(v -> {
            unlockDialog.dismiss();
            if (!reUnlock)
                userUnlockedFlag = true;
            this.finish();
        });

        unlockDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(unlockDialog!=null && unlockDialog.isShowing())
            unlockDialog.dismiss();
    }
}
