package hr.ferit.student.smartlock;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ReturnMapActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMarkerDragListener {

    private GoogleMap mMap;
    LocationHelper locationHelper;
    String bikeId;
    LatLng bikeLocation;
    MarkerOptions marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_map);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        bikeId = getIntent().getStringExtra(getString(R.string.bike_id_intent));
        bikeLocation = getIntent().getParcelableExtra(getString(R.string.bike_location_intent));

        setupToolbar();
    }

    private void showInfoDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Set SmartLock location")
                .setMessage(getString(R.string.return_map_marker_info))
                .setPositiveButton("OK", (dialog, which) -> dialog.dismiss())
                .show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(45.53, 18.7)));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(14));

        setupMarker();
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMarkerDragListener(this);

        locationHelper = new LocationHelper(this, mMap);
        locationHelper.checkPermissions();
        locationHelper.checkGPS(this);
        locationHelper.updateMarkerLocation(marker, true);

        mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));

        showInfoDialog();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        locationHelper.onRequestPermissionsResult(requestCode, grantResults);
    }

    private void setupMarker() {
        marker = new MarkerOptions()
                .position(bikeLocation)
                .title(bikeId)
                .draggable(true)
                .icon(BitmapDescriptorFactory.fromBitmap(getBitmap()));
        mMap.addMarker(marker);
        mMap.setOnMarkerClickListener(marker1 -> {
            Toast.makeText(this, "Press and hold marker to drag it to new location", Toast.LENGTH_LONG).show();
            return true;
        });
    }

    private Bitmap getBitmap() {
        Drawable drawable = getResources().getDrawable(R.drawable.ic_bike_marker);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private void setupToolbar(){
        Toolbar returnMapToolbar = findViewById(R.id.returnMapToolbar);
        setSupportActionBar(returnMapToolbar);

        TextView btnToolbarCancel = findViewById(R.id.btnToolbarCancel);
        TextView btnToolbarDone = findViewById(R.id.btnToolbarDone);

        btnToolbarCancel.setOnClickListener(v -> this.finish());
        btnToolbarDone.setOnClickListener(v -> {
            removeUserBike();
            Toast.makeText(this, "SmartLock Bike returned successfully!", Toast.LENGTH_LONG).show();
            this.finish();
        });
    }

    private void removeUserBike() {
        DatabaseReference dbReference = FirebaseDatabase.getInstance()
                .getReference("users").child(FirebaseAuth.getInstance().getUid())
                .child("bikes").child(bikeId);
        dbReference.setValue(null);
        updateBikes();
    }

    private void updateBikes(){
        System.out.println(marker.getPosition());
        DatabaseReference dbRef = FirebaseDatabase.getInstance()
                .getReference("bikes").child(bikeId);
        dbRef.child("user").setValue(null);
        dbRef.child("location").setValue(marker.getPosition());
    }

    @Override
    public boolean onMyLocationButtonClick() {
        locationHelper.checkGPS(this);
        locationHelper.updateMarkerLocation(marker, false);
        return false;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        this.marker.position(marker.getPosition());
    }
}
