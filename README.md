# SmartLock

<img src="app/src/main/ic_launcher-web.png" title="SmartLock logo" width="100" height="100">


- Project is developed within Internet of Things course `@FERIT`
- `SmartLock` is proof-of-concept student project - an Android application and a smart lock hardware for renting out and sharing city bikes. Application uses `Google Firebase` as cloud datastore and `Bluetooth` to communicate with smart lock hardware - `Arduino Uno` with solenoid.
- **University Rector's Award**
- For details please read technical documentation: [SmartLock - technical documentation.pdf](docs/SmartLock - tehnička dokumentacija.pdf)
